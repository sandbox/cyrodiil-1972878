
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Developer: Raphael Dürst <raphael.duerst@innoveto.com>

The OpenID single provider module lets you configure a OpenID provider which
will always be used. Instead of a normal login block, the user will only see
a "Login to OpenID" button on the login block which will redirect him to the
OpenID provider.


INSTALLATION
------------

1. This module requires the the openid module.
   Make sure that the module is enabled.

2. Copy this openid_single_provider/ directory to your sites/SITENAME/modules
   directory.

3. Enable the module in admin/modules

4. Configure the module in admin/config/people/openid-single-provider


USAGE
-----

When the module is enabled, the normal login block will be replaced with a
"Login to OpenID" button. By clicking on the button, the user will be
redirected to the OpenID provider specified in the module configuration.

An anonymous user will be automatically redirected to the OpenID provider
when going to user/.

If the OpenID user is associated with a Drupal user, the user will be
logged in with the appropriate Drupal user.

To log in to drupal without using OpenID, you have to go to login/direct.
